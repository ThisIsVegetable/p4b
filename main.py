import urllib.request
import os
from PIL import Image

path = os.getcwd()+"/source/"
fileName = "mojav.jpg"
saveFileName = "mojavSave.jpg"

url = "http://media.idownloadblog.com/wp-content/uploads/2018/06/macOS-Mojave-Day-wallpaper.jpg"


if not os.path.isdir(path):
    os.makedirs(path)

urllib.request.urlretrieve(url, path+fileName)

imageMojav = Image.open(path+fileName)
imageSize = imageMojav.size
width = imageSize[0]
height = imageSize[1]

imageMojav = imageMojav.resize((1920, int(height*1920/width)))
imageMojav = imageMojav.transpose(Image.FLIP_LEFT_RIGHT)

width = imageMojav.size[0]
height = imageMojav.size[1]
centerX = width/2;
centerY = height/2;
coordinate = (int(centerX-320), int(centerY-320), 1280, 860)

imageMojav = imageMojav.crop(coordinate)
imageMojavRGB = imageMojav.convert("RGB")
im = imageMojavRGB.load()

for i in range(1, 640):
    for j in range(1, 640):
        color = im[i,j]
        im[i, j] = (255-color[0], color[1], color[2])

imageMojavRGB.save(saveFileName)



